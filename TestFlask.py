# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, session, redirect, url_for

import sqlite3

import nltk
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

import string
import datetime

app = Flask(__name__)
app.config['TESTING'] = True
DB_NAME = "users.db"

@app.route('/',methods = ['POST', 'GET'])
def login():
	
	data = {'company': 'Trin'}
	
	if request.method == 'POST': #Si llego por POST es que le he mandado algo a la pagina (POST SE USA PARA MANDAR DE CLIENTE A SERVIDOR )
		
		result = request.form #Result se maneja como un dicionarioclear
		now = datetime.datetime.now()
		
		if checkLogin(result) == True:
			saveLogin(now,request.remote_addr,result['name'],True)
			session['username'] = result['name']
			session['logged'] = True
			return redirect(url_for('home'))
		
		else:
			saveLogin(now,request.remote_addr,result['name'],False)
			return render_template('Error.html')
	
	#Entra por aqui poniendo la dirección en el navegador
	if request.method == 'GET': #Si llego por GET es que no le mando nada y simplemente vuelvo a ella (GET SE USA PARA MANDAR DE SERVIDOR A CLIENTE)
		
		return render_template('Login.html', title='Bienvenido', data=data)

@app.route('/home',methods = ['POST', 'GET','HEAD'])
def home():
	
	#if request.method == 'POST':

	#	result = request.form #Result se maneja como un dicionario
	#	return render_template('Home.html', title='Home', result=result, name = session['username'] )

	#else:
	#	return render_template('Home.html', title='Home', name = session['username'] )
	return render_template('Home.html', title='Home', name = session['username'] )


@app.route('/stemming',methods = ['POST', 'GET'])
def stemming():
	if request.method == 'POST':
		result = request.form #Result se maneja como un dicionario
		#print(result['textToPNL'])
		res = doPreprocess(result['textToPNL'])
		return render_template('Stemming.html', title='Test', result=result, stemming_result=res)
	else:
		return "Error..."

@app.route('/search',methods = ['POST', 'GET'])
def search():
	return render_template('Search.html', title='Test Search')

@app.route('/results',methods = ['POST', 'GET'])
def results():
	if request.method == 'POST':
		result = request.form 
		return render_template('Results.html', title='Test Results', result=result)

def saveLogin(now,ip,name,result):
	con_bd = sqlite3.connect(DB_NAME)
	cursor = con_bd.cursor()
	
	#Ejemplo "INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)"
	sql = 'INSERT INTO logins VALUES (null,\"'+str(now)+'\",\"'+str(ip)+'\",\"'+str(name)+'\",\"'+str(result)+'\");'
	 
	cursor.execute(sql)
	con_bd.commit()
	
	cursor.close()
	con_bd.close()


def doPreprocess(text):

	tokens = nltk.word_tokenize(text)

    #Normalizo el texto (Todo a minusculas)
	tokens = [token.lower() for token in tokens]

	#1º - Eliminar signos de puntuación
	#print(string.punctuation) #Todos los signos de puntuación estan aqui
	for i in tokens:
		if i in string.punctuation:
			tokens.remove(i)

	#2º - Eliminar stopwords
	#print(stopwords.words('spanish')) # Lista de stopwords
	for i in tokens:
		if i in stopwords.words('spanish'):
			tokens.remove(i)

	#3º - Steaming? Para Todo?
	stemmer_1 = SnowballStemmer("spanish") # Choose a language
	tokens = [stemmer_1.stem(i) for i in tokens] 
	
	res = ""
	for i in tokens:
		res+=str(i+" ")
    
	return res


def checkLogin(result):
	res = False

	consult = consultDB("SELECT * FROM users;")
	
	for i in consult:
		if result['name'] in i:
			if result['password'] == i[2]:
				res = True

	return res

#Lanzar consultas contra la base de datos
def consultDB(sql):
    res = []
    
    con_bd = sqlite3.connect(DB_NAME)
    cursor = con_bd.cursor()
    cursor.execute(sql)

    for i in cursor:
        res.append(i)

    cursor.close()
    con_bd.close()

    return res

def main():
	app.secret_key = 'root'
	app.run(host='0.0.0.0', port=5000, debug = False)

if __name__ == '__main__':
	main()